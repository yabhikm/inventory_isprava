import React from 'react';
import PropTypes from 'prop-types';
import CssBaseline from '@material-ui/core/CssBaseline';

import Header from './components/Header';

class InventoryApp extends React.Component {

  render() {
    return (
      <React.Fragment>
        <CssBaseline />
        <Header />
        {this.props.children}
      </React.Fragment>
    );
  }
}

InventoryApp.propTypes = {
  children: PropTypes.node.isRequired,
};

export default InventoryApp;
