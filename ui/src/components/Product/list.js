import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Grid,
  Typography
} from '@material-ui/core';

import api from '../../api';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  table: {
    minWidth: 650,
  },
  colorRed: {
    color: 'red'
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  }
}));

const getWarehouse = () => {
  return api.get('/warehouse');
}

const getProducts = () => {
  return api.get('/products');
}

const loadData = () => {
  return new Promise((resolve, reject) => {
    const gridData = {
      columns: [
        {
          name: 'SKU Code',
          key: 'sku_code'
        },
        {
          name: 'Product Name',
          key: 'product_name'
        }
      ],
      rows: []
    };

    getWarehouse().then(res => {
      const { status = null, data = [] } = res;
      if (status && status === 200) {
        data.forEach(item => {
          gridData.columns.push({
            name: item.name,
            key: item.wh_code
          });
        });
      }

      getProducts().then(res => {
        const { status = null, data = {} } = res;
        if (status && status === 200) {
          for (let key in data) {
            if (data.hasOwnProperty(key)) {
              const rows = {
                id: data[key].id
              };
              gridData.columns.forEach(item => {
                rows[item.key] = data[key][item.key]
              });
              gridData.rows.push(rows);
            }
          }
          resolve(gridData);
        }
      }).catch(err => reject(err));
    }).catch(err => reject(err));
  });
}

const List = () => {
  const classes = useStyles();
  const [products, setProducts] = useState([]);
  const [columns, setColumn] = useState([]);

  useEffect(() => {
    loadData().then(res => {
      const { columns = [], rows = [] } = res;
      setProducts(rows);
      setColumn(columns);
    }).catch(err => {
      setProducts([]);
      setColumn([]);
    });
  }, []);

  return (
    <div className={classes.root}>
      <Grid item xs={12}>
        <Paper className={classes.paper}>
          <Typography variant="h6">
            Product List
          </Typography>
          <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="simple table">
              <TableHead>
                <TableRow>
                  {columns.map(item => (
                    <TableCell key={item.key}>{ item.name }</TableCell>  
                  ))}
                </TableRow>
              </TableHead>
              <TableBody>
                {products.map(product => (
                  <TableRow key={product.sku_code}>
                    {columns.map((item, index) => (
                      ['sku_code', 'product_name'].includes(item.key)
                      ? <TableCell key={index}>{(item.key === 'sku_code' && <Link to={`/product/${product.id}`}>{product[item.key]}</Link>) || product[item.key]}</TableCell>
                      : <TableCell key={index} className={product[item.key].count >= product[item.key].min_threshold ? '' : classes.colorRed}>{product[item.key].count}</TableCell>
                    ))}
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Paper>
      </Grid>
    </div>
  );
}

export default List;