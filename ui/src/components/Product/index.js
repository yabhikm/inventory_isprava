import List from './list';
import Edit from './edit';

export {
  List,
  Edit
};