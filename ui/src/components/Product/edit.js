import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Grid,
  Paper,
  Typography,
  Button,
  TextField,
  FormLabel,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TableFooter
} from '@material-ui/core';
import { Alert } from '@material-ui/lab';

import api from '../../api';
import { withRouter } from 'react-router-dom';
import './styles.css';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  form: {
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
      width: 200,
    }
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  table: {
    minWidth: 650,
  }
}));

const fields = {
  wh_code: {
    ref: null,
    value: '',
    isError: false
  },
  wh_name: {
    ref: null,
    value: '',
    isError: false
  },
  pincode: {
    ref: null,
    value: '',
    isError: false
  }
};

const getProduct = id => {
  return api.get('/product/:id', { params: id });
}

const Edit = ( { match }) => {
  const { id = null } = match.params || {};

  const classes = useStyles();
  const [stateFields, setStateFields] = useState(fields);
  const [alert, setAlert] = useState({show: false, severity: '', message: ''});
  const [product, setProduct] = useState({wh_count: []});

  useEffect(() => {
    getProduct(id).then(res => {
      const { data } = res;
      const prod = {
        id: data.detail.id,
        product_name: data.detail.product_name,
        sku_code: data.detail.sku_code,
        wh_count: data.warehouse_count.map(i => i)
      };
      setProduct({...prod});
    }).catch(err => {

    });
  }, []);

  const onValueChange = (event, wh_id, field) => {
    const value = event.target.value;
    const { wh_count } = product;
    const index = wh_count.findIndex(i => i.wh_id === wh_id);
    if (wh_count[index]) {
      wh_count[index][field] = +value;
    }
    setProduct({
      ...product,
      wh_count
    });
  }

  const updateInventory = () => {
    const { wh_count } = product;
    api.patch('/product/:id', wh_count, {params: product.id}).then(res => {
      setAlert({
        show: true,
        severity: 'success',
        message: 'Product count updated successfully!!'
      });
    }).catch(err => {
      setAlert({
        show: true,
        severity: 'error',
        message: 'Oops! Something went wrong'
      });
    });
  }

  return (
    <div className={classes.root}>
      {alert.show && (<Alert severity={alert.severity}>
        {alert.message}
      </Alert>)}
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Paper className={classes.paper}>
            <Typography variant="h6">
              Product Inventory - <b>{product.sku_code}</b>
            </Typography>
          </Paper>
          <Paper className={classes.paper}>
            <TableContainer component={Paper}>
              <Table className={classes.table} aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell>WareHouse</TableCell>
                    <TableCell>Count</TableCell>
                    <TableCell>Minimum Threshold</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {product.wh_count.map(i => (
                    <TableRow key={i.wh_id}>
                      <TableCell>{i.wh_name}</TableCell>
                      <TableCell>
                        <TextField
                          id={`count${i.wh_id}`}
                          value={i.count}
                          type="number"
                          onChange={(e) => onValueChange(e, i.wh_id, 'count')}
                        />
                    </TableCell>
                      <TableCell>
                        <TextField
                          id={`min_threshold${i.wh_id}`}
                          value={i.min_threshold}
                          type="number"
                          onChange={(e) => onValueChange(e, i.wh_id, 'min_threshold')}
                        />
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
                <TableFooter>
                  <TableRow>
                    <TableCell colSpan={3} align="center">
                      <Button variant="contained" color="primary" onClick={updateInventory}>
                        Update
                      </Button>
                    </TableCell>
                  </TableRow>
                </TableFooter>
              </Table>
            </TableContainer>
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
}

export default withRouter(Edit);