import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Grid,
  Paper,
  Typography,
  Button,
  TextField
} from '@material-ui/core';
import { Alert } from '@material-ui/lab';

import api from '../../api';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  form: {
    '& .MuiTextField-root': {
      margin: theme.spacing(3),
      width: 300,
    }
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
}));

const fields = {
  wh_code: {
    ref: null,
    value: '',
    isError: false
  },
  wh_name: {
    ref: null,
    value: '',
    isError: false
  },
  pincode: {
    ref: null,
    value: '',
    isError: false
  }
};

const getWarehouseByCode = code => {
  return api.get('/warehouse/:code', { params: code });
}

const WarehouseAdd = () => {
  const classes = useStyles();
  const [stateFields, setStateFields] = useState(fields);
  const [alert, setAlert] = useState({show: false, severity: '', message: ''});

  const validateForm = () => {
    const fields = stateFields;
    for (let key in fields) {
      if (fields.hasOwnProperty(key)) {
        fields[key].isError = false;
        const value = fields[key].ref.value || null;
        if (value) {
          fields[key].value = value;
        } else {
          fields[key].isError = true;
        }
      }
    }

    if (!fields.pincode.isError && (fields.pincode.length < 6 )) {
      fields.pincode.isError = true;
    }

    if (!fields.wh_code.isError) {
      if (fields.wh_code.value.length < 4 || fields.wh_code.value.length > 16) {
        fields.wh_code.isError = true;
      } else {
        getWarehouseByCode(fields.wh_code.value).then(res => {
          fields.wh_code.isError = true;
          setStateFields({...fields});
        }).catch(err => {
          fields.wh_code.isError = false;
          setStateFields({...fields});
          addWarehouse();
        });
      }
    }
  }

  const addWarehouse = () => {
    const fields = stateFields;
    let isError = false;
    for (const key in fields) {
      if (fields.hasOwnProperty(key)) {
        isError = fields[key].isError;
        if (isError) {
          break;
        }
      }
    }

    if (isError) {
      return;
    }

    api.post('/warehouse', {
      wh_code: fields.wh_code.value,
      name: fields.wh_name.value,
      pincode: fields.pincode.value
    }).then(res => {
      fields.wh_code.ref.value = '';
      fields.wh_name.ref.value = '';
      fields.pincode.ref.value = '';
      setAlert({
        show: true,
        severity: 'success',
        message: 'Warehouse added successfully!!'
      });
    }).catch(err => {
      fields.wh_code.ref.value = '';
      fields.wh_name.ref.value = '';
      fields.pincode.ref.value = '';
      setAlert({
        show: true,
        severity: 'error',
        message: 'Oops! Something went wrong'
      });
    });
  }

  return (
    <div className={classes.root}>
      {alert.show && (<Alert severity={alert.severity}>
        {alert.message}
      </Alert>)}
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Paper className={classes.paper}>
            <Typography variant="h6">
              Add Warehouse
            </Typography>
          </Paper>
          <Paper className={classes.paper}>
            <form className={classes.form} noValidate autoComplete="off">
              <div>
                <TextField
                  error={stateFields.wh_code.isError}
                  id="wh_code"
                  label="Warehouse Code"
                  helperText={stateFields.wh_code.isError ? "Incorrect entry." : ''}
                  inputRef={node => stateFields.wh_code.ref = node}
                />

                <TextField
                  error={stateFields.wh_name.isError}
                  id="wh_name"
                  label="Warehouse Name"
                  helperText={stateFields.wh_name.isError ? "Incorrect entry." : ''}
                  inputRef={node => stateFields.wh_name.ref = node}
                />
              </div>
              <div>
                <TextField
                  error={stateFields.pincode.isError}
                  id="pincode"
                  label="Pincode"
                  helperText={stateFields.pincode.isError ? "Incorrect entry." : ''}
                  inputRef={node => stateFields.pincode.ref = node}
                />
              </div>
            </form>
          </Paper>

          <Paper className={classes.paper}>
            <Button variant="contained" color="primary" onClick={validateForm}>
              Add
            </Button>
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
}

export default WarehouseAdd;