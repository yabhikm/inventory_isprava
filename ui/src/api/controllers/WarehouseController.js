import requestService from '../services/requestService';

const getWarehouse = query => {
  const request = {
    method: 'get',
    url: `/warehouse`,
    params: {
      type: 'warehouse_list',
    }
  };

  return requestService(request);
};

const getWarehouseByCode = query => {
  const request = {
    method: 'get',
    url: `/warehouse/${query}`,
    params: {
      type: 'warehouse_bycode',
    }
  };
  return requestService(request);
}

const addWarehouse = (data, query) => {
  const request = {
    method: 'post',
    url: `/warehouse`,
    params: {
      type: 'add_warehouse',
    },
    data
  };
  return requestService(request);
}

export default {
  getWarehouse,
  getWarehouseByCode,
  addWarehouse
};
