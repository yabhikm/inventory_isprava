import requestService from '../services/requestService';

const getProducts = query => {
  const request = {
    method: 'get',
    url: `/product`,
    params: {
      type: 'product_list',
    }
  };

  return requestService(request);
};

const getProduct = query => {
  const request = {
    method: 'get',
    url: `/product/${query}`,
    params: {
      type: 'get_product',
    }
  };
  return requestService(request);
}

const updateWarehouseCount = (data, query) => {
  const request = {
    method: 'patch',
    url: `/product/${query}`,
    params: {
      type: 'update_product_count',
    },
    data
  };
  return requestService(request);
}

export default {
  getProducts,
  getProduct,
  updateWarehouseCount
};
