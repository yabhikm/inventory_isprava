import ProductController from './controllers/ProductController';
import WarehouseController from './controllers/WarehouseController';

const routes = {
  get: {},
  post: {},
  put: {}
};

// GET
routes.get['/products'] = ProductController.getProducts;
routes.get['/product/:id'] = ProductController.getProduct;
routes.get['/warehouse'] = WarehouseController.getWarehouse;
routes.get['/warehouse/:code'] = WarehouseController.getWarehouseByCode;

// POST
routes.post['/warehouse'] = WarehouseController.addWarehouse;

///PUT
routes.put['/product/:id'] = ProductController.updateWarehouseCount;

export default routes;
