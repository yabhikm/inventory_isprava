import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import InventoryApp from './InventoryApp';
import { List, Edit } from './components/Product';
import Warehouse from './components/Warehouse';

function App() {
  return (
    <Router>
      <InventoryApp>
        <Switch>
          <Route path="/" exact component={List} />
          <Route path="/product/:id" exact component={Edit} />
          <Route path="/warehouse" exact component={Warehouse} />
        </Switch>
      </InventoryApp>
    </Router>
  );
}

export default App;
