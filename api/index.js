const express = require('express')
const app = express()
const port = 3001
const bodyParser = require('body-parser')

const indexRoute = require('./routes/index');

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "*");
    res.header('Access-Control-Allow-Methods', 'PATCH, POST, GET, DELETE, OPTIONS');
    next();
});

app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: false
}));

// Logging Request's
app.use((req, res, next) => {
  console.log(`${req.method}: ${req.url}`, req.body);
  next();
});

app.use('/api', indexRoute);
app.get('/ping', (req, res) => res.send('Server Active...'))
// catch 404 and forward to error handler
app.use((req, res, next) => {
  const code = 404;
  const message = 'Not Found';
  console.log(`Response: ${code}`, message);
  res.status(code).send(message);
});

// error handler
app.use((err, req, res, next) => {
  console.log('Error=====>', err);
  res.status(err.status || 500).json({
    message:'Something broke!',
    error: req.app.get('env') === 'development' ? err : {}
  });
});

app.listen(port, () => console.log(`Inventory API is listening on port ${port}!`))