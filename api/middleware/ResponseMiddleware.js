class ResponseMiddleware {
    /**
     * ResponseMiddleware to handle response from common source
     */
    constructor() { }

    sendResponse = (req, res, next) => {
        const code = res.code || 500;
        const body = res.body ||
        {
            message: 'Something broke!',
        };
        console.log(`Response: ${code}`, JSON.stringify(body));
        res.status(code).json(body);
    }
}

module.exports = ResponseMiddleware;