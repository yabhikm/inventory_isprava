const express = require('express');
const router = express.Router();

const ResponseMiddleware = require('../middleware/ResponseMiddleware');
const WareHouseController = require('../controllers/WareHouseController');

let wareHouseController = new WareHouseController();
let responseMiddleware = new ResponseMiddleware();
router.get(
    '/',
    (req, res, next) => wareHouseController.getWareHouse(req, res, next),
    responseMiddleware.sendResponse
);

router.get(
    '/:code',
    (req, res, next) => wareHouseController.getWareHouseByCode(req, res, next),
    responseMiddleware.sendResponse
);

router.post(
    '/',
    (req, res, next) => wareHouseController.createWareHouse(req, res, next),
    responseMiddleware.sendResponse
);

module.exports = router;
