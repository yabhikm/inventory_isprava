const express = require('express');
const app = express();

const productRoute = require('./product');
const warehouseRoute = require('./warehouse');

app.use('/product', productRoute);
app.use('/warehouse', warehouseRoute);
module.exports = app;