const express = require('express');
const router = express.Router();

const ResponseMiddleware = require('../middleware/ResponseMiddleware');
const ProductController = require('../controllers/ProductController');

let productController = new ProductController();
let responseMiddleware = new ResponseMiddleware();
router.get(
    '/',
    (req, res, next) => productController.getProducts(req, res, next),
    responseMiddleware.sendResponse
);

router.get(
    '/:product_id',
    (req, res, next) => productController.getProduct(req, res, next),
    responseMiddleware.sendResponse
);

router.patch(
    '/:product_id',
    (req, res, next) => productController.updateProductCount(req, res, next),
    responseMiddleware.sendResponse
);

module.exports = router;
