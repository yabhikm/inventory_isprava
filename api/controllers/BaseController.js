const DB = require('../config/db');

class BaseController {
    /**
     * BaseController Class to handle all product related requests
     */
    db = DB;
    constructor() {}
}

module.exports = BaseController;