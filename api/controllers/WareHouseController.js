const BaseController = require('./BaseController');

class WareHouseController extends BaseController {
    /**
     * WareHouseController.js Class to handle all warehouse related requests
     */

    constructor() {
        super();
    }

    /**
     * `getWareHouse` function to get wareHouse 
     */
    getWareHouse = (req, res, next) => {
        this.db.query("select * from ware_house", (err, rows) => {
            if (err) {
                throw err;
            }

            res.code = 200;
            res.body = rows;
            next();
        });
    }

    getWareHouseByCode = (req, res, next) => {
        const { code = null } = req.params;
        this.selectWareHouse(code).then(data => {
            if (data.hasOwnProperty('wh_code')) {
                res.code = 200;
                res.body = data;
            } else {
                res.code = 404;
                res.body = {};
            }
            next();
        }).catch(err => {throw err;});
    }

    /**
     * `createWareHouse` function to get wareHouse 
     */
    createWareHouse = (req, res, next) => {
        const { wh_code, name, pincode } = req.body;
        this.selectWareHouse(wh_code).then(data => {
            if (data.hasOwnProperty('wh_code')) {
                res.code = 409;
                res.body = {};
                next();
            } else {
                const ware_house = {
                    wh_code,
                    name,
                    pincode
                };
                this.db.query('insert into ware_house set ?', ware_house, (err, result) => {
                    if (err) {
                        throw err;
                    }
                    const { insertId } = result;

                    this.getProductIds().then(productIds => {
                        productIds.forEach(i => {
                            const product_wh_count = {
                                wh_id: insertId,
                                product_id: i.id,
                                count: 0,
                                min_threshold: 10
                            }
                            this.db.query('insert into product_wh_count set ?', product_wh_count, (err, result) => {});
                        });
                        res.code = 201;
                        res.body = {
                            id: insertId,
                            ...ware_house
                        };
                        next();
                    });
                });
            }
        }).catch(err => {throw err;});
    }

    selectWareHouse = code => {
        return new Promise((resolve, reject) => {
            this.db.query("select * from ware_house where wh_code = ? limit 1", [code], (err, rows) => {
                if (err) {
                    reject(err);
                    return;
                }

                resolve(rows[0] || {});
            });
        });
    }

    getProductIds = () => {
        return new Promise((resolve, reject) => {
            this.db.query('select id from product', (err, rows) => {
                if (err) {
                    return reject(err);
                }
                resolve(rows);
            });
        });
    }
}

module.exports = WareHouseController;