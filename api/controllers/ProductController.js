const BaseController = require('./BaseController');

class ProductController extends BaseController {
    /**
     * ProductController Class to handle all product related requests
     */

    constructor() {
        super();
    }

    /**
     * `getProducts` function to get all products 
     */
    getProducts = (req, res, next) => {
        this.db.query("select P.*, C.count, C.min_threshold, W.id as wh_id, W.wh_code, W.name as wh_name from product as P join product_wh_count as C on P.id = C.product_id join ware_house as W on C.wh_id = W.id group by C.product_id, C.wh_id", (err, rows) => {
            if (err) {
                throw err;
            }
            const products = {};

            rows.forEach(item => {
                if (products[item.sku_code]) {
                    products[item.sku_code][item.wh_code] = {
                        count: item.count,
                        min_threshold: item.min_threshold
                    }
                } else {
                    products[item.sku_code] = {
                        id: item.id,
                        sku_code: item.sku_code,
                        product_name: item.name,
                    };
                    products[item.sku_code][item.wh_code] = {
                        count: item.count,
                        min_threshold: item.min_threshold
                    }
                }
            });

            res.code = 200;
            res.body = products;
            next();
        });
    }

    /**
     * `getProduct` function to get single products 
     */
    getProduct = (req, res, next) => {
        const { product_id = null } = req.params;
        if (!product_id) {
            return next();
        }
        this.db.query("select P.*, C.count, C.min_threshold, W.id as wh_id, W.wh_code, W.name as wh_name from product as P join product_wh_count as C on P.id = C.product_id join ware_house as W on C.wh_id = W.id where P.id = ? group by C.product_id, C.wh_id", [product_id], (err, rows) => {
            if (err) {
                throw err;
            }
            const products = {
                detail: {},
                warehouse_count: []
            };

            rows.forEach(item => {
                products.detail = {
                    id: item.id,
                    sku_code: item.sku_code,
                    product_name: item.name
                }

                products.warehouse_count.push({
                    wh_name: item.wh_name,
                    wh_code: item.wh_code,
                    wh_id: item.wh_id,
                    count: item.count,
                    min_threshold: item.min_threshold
                });
            });

            res.code = 200;
            res.body = products;
            next();
        });
    }

    updateProductCount = (req, res, next) => {
        const wh_count = req.body;

        wh_count.forEach(i => {
            const obj = {
                count: i.count,
                min_threshold: i.min_threshold
            };
            this.db.query('update product_wh_count set ? where id = ?', [obj, i.wh_id], (err, result) => {
                if (err) {
                    throw err;
                }
            });            
        });

        res.code = 200;
        res.body = {};
        next();
    }
}

module.exports = ProductController;