const mysql = require('mysql');

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'inventory_management_isprava'
});

connection.connect(err => {
    if(err) throw err;
});

module.exports = connection;